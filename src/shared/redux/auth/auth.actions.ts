import { Profile } from './../../types';
import { AuthActionTypes, FetchProfileAction, FetchProfileErrorAction, FetchProfileSuccessAction, LoginAction, LoginErrorAction, LoginSuccessAction } from './auth.types';


export const startLogin = (username: string, password: string): LoginAction => {
    return {
        type: AuthActionTypes.LOGIN,
        payload: {username, password}
    }
}

export const startLoginSuccess = (): LoginSuccessAction => {
    return {
        type: AuthActionTypes.LOGIN_SUCCESS,
    }
}

export const startLoginError = (error: string): LoginErrorAction => {
    return {
        type: AuthActionTypes.LOGIN_ERROR,
        payload: error
    }
}


export const fetchProfile = (): FetchProfileAction => {
    return {
        type: AuthActionTypes.FETCH_PROFILE,
    }
}

export const fetchProfileSuccess = (profile: Profile): FetchProfileSuccessAction => {
    return {
        type: AuthActionTypes.FETCH_PROFILE_SUCCESS,
        payload: profile,
    }
}

export const fetchProfileError = (error: string): FetchProfileErrorAction => {
    return {
        type: AuthActionTypes.FETCH_PROFILE_ERROR,
        payload: error,
    }
}