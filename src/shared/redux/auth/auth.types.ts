import { Profile } from './../../types';

export enum  AuthActionTypes {
    LOGIN = 'LOGIN',
    LOGIN_SUCCESS = 'LOGIN_SUCCESS',
    LOGIN_ERROR = 'LOGIN_ERROR',
    FETCH_PROFILE = 'FETCH_PROFILE',
    SET_PROFILE = 'SET_PROFILE',
    FETCH_PROFILE_SUCCESS = 'FETCH_PROFILE_SUCCESS',
    FETCH_PROFILE_ERROR = 'FETCH_PROFILE_ERROR'
}

export interface LoginAction {
    type:  AuthActionTypes.LOGIN;
    payload: {
        username: string,
        password: string;
    }
}

export interface LoginSuccessAction {
    type:  AuthActionTypes.LOGIN_SUCCESS;
}

export interface LoginErrorAction {
    type:  AuthActionTypes.LOGIN_ERROR;
    payload: string;
}

export interface FetchProfileAction {
    type:  AuthActionTypes.FETCH_PROFILE;
}

export interface SetProfileAction {
    type:  AuthActionTypes.SET_PROFILE;
    payload: Profile;
}

export interface FetchProfileSuccessAction {
    type:  AuthActionTypes.FETCH_PROFILE_SUCCESS;
    payload: Profile;
}

export interface FetchProfileErrorAction {
    type:  AuthActionTypes.FETCH_PROFILE_ERROR;
    payload: string;
}

export type AuthAction = FetchProfileAction 
    | SetProfileAction 
    | FetchProfileSuccessAction 
    | FetchProfileErrorAction
    | LoginAction
    | LoginSuccessAction
    | LoginErrorAction;