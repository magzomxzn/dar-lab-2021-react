import React from 'react';
import { Link } from 'react-router-dom';
import { getFormattedDayAndMonth } from '../../shared/dateformat';
import { Article } from '../../shared/types';

import styles from './ArticleItem.module.scss';


type Props = {
  article: Article,
}

const ArticleItem: React.FunctionComponent<Props> = ({
                                                       article
                                                     }) => {
  const formatted_creation_date = getFormattedDayAndMonth(article.created_at);
  return (
    <div className={styles.ArticleListContainer}>
        <Link to={`/article/${article.id}`}>
          <article className={styles.ArticleList}>
            <div className={styles.article_content}>
              <h3 className={styles.article_content_title}>{article.title}</h3>
            
              <p className={styles.article_content_annotation}>{article.annotation}</p>
              <div className={styles.article_content_footer}>
                <span className={styles.article_content_date}>{formatted_creation_date}</span>
              </div>
            </div>
            <div className={styles.article_image}>
              <img src={`https://dev-darmedia-uploads.s3.eu-west-1.amazonaws.com/${article.image}`} />
            </div>
          </article>
        </Link>
    </div>
  )
};


export default ArticleItem;

