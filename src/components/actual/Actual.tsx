import React, {} from 'react';
import styles from './Actual.module.scss';
import ArticleItem from '../article-item/ArticleItem';
import { Article } from '../../shared/types';

type Props = {
  articles: Article[]
}

export const ArticlesList: React.FunctionComponent<Props> = ({articles}) => {

  return (
    <div className={styles.ActualContainer}>
      <div className={styles.Actual}>
        <h4 className={styles.actual_header}>Актуальное</h4>
          <div className={styles.actual}>
            {articles.map(article => (
              <ArticleItem key={article.id} article={article} />
            ))}
          </div>
      </div>
    </div>
  )
};
