import React, { useEffect, useRef, useState } from 'react';
import { checkLikes } from '../../shared/utils';

import styles from './Counter.module.scss';

type Props = {
    initialCount: number;
}

const Counter: React.FC<Props> = ({initialCount}) => {

    const [count, setCount] = useState<number>(initialCount);
    const [message, setMessage] = useState<string>('');
    const [error, setError] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const initialLoad = useRef<boolean>(false);
    const divRef = useRef(null);

    useEffect(() => {
        if (!initialLoad.current) {
            initialLoad.current = true;
            return;
        }
        clearMessages();
        setLoading(true);
        const checkAsync = async () => {
            try {
                const res = await checkLikes(count);
                setMessage(res);
            } catch(err) {
                setError(err);
            }
            setLoading(false);
        }
        checkAsync();
    }, [count])

    const clearMessages = () => {
        setMessage('');
        setError('');
    }

    return (
        <div className={styles.wrapper}>
            <div ref={divRef} className={styles.count}>{count}</div>
            <div className={styles.controls}>
                <button 
                    className={styles.button} 
                    disabled={loading}
                    onClick={() => setCount(count => count - 1)}>-</button>
                <button 
                    className={styles.button} 
                    disabled={loading}
                    onClick={() => setCount(count => count + 1)}>+</button>
            </div>
            {message && <div className={styles.success}>{message}</div>}
            {error ? <div className={styles.error}>{error}</div> : ''}
            {loading && <div>Loading...</div>}
        </div>
    )
}

export default Counter;