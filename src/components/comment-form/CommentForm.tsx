import React, { useEffect, useState } from 'react';
import AppContext from '../../shared/app.context';
import Button from '../button/Button';
import Profile from '../profile/Profile';
import styles from './CommentForm.module.scss';

const CommentForm: React.FC = () => {

    const [fields, setFields] = useState<{
        username: string;
        email: string;
        comment: string;
    }>({
        username: '',
        email: '',
        comment: ''
    });


    const onSubmit = () => {
        console.log('Form submitted', fields)
    }

    useEffect(() => {
        // TODO: validate form fields!
        console.log('FORM CHANGED', fields)
    }, [fields])

    const fieldChange = (fieldName: string, value: any) => {
        setFields(oldState => ({
            ...oldState,
            [fieldName]: value
        }));
    }

    return (
        <AppContext.Consumer>
        {({state: {profile}}) => profile ? 
            (<div className={styles.wrapper}>
                <div className={styles.user}>
                    <div className={styles.formControl}>
                        <label>Username</label>
                        <input value={fields.username} name="username" type="text" onChange={e => fieldChange('username', e.target.value)} />
                    </div>
                    <div className={styles.formControl}>
                        <label>Email</label>
                        <input name="email" type="text" value={fields.email} onChange={e => fieldChange('email', e.target.value)} />
                    </div>
                    <Profile username={profile.username} avatar={profile.avatar} />
                </div>
                <div className={styles.comment}>
                    <div className={styles.formControl}>
                        <label>Comment</label>
                        <textarea name="comment" value={fields.comment} onChange={e => fieldChange('comment', e.target.value)} />
                    </div>
                </div>
                <div className={styles.controls}>
                    <Button title="Submit" variant="primary" onClick={onSubmit} />
                </div>
            </div>
            ) : <div className={styles.wrapper}>Чтобы оставить комментарий войдите в аккаунт</div> }
        </AppContext.Consumer>
    )
}

export default CommentForm;