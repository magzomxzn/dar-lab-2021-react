import React, {  } from 'react';
import { Link } from 'react-router-dom';
import { Category } from '../../../shared/types';
import classes from './HeaderBottom.module.scss';

type Props = {
  items: Category[];
}

const HeaderBottom: React.FC<Props> = ({items}) => {

  return (
    <div className={classes.header_bottom}>
      <div className={classes.header_bottom_main}>
        <ul>
          <li className={classes.nav_link}><Link to="/articles">Все статьи</Link></li>
          {
            items.map(item => (<li key={item.id} className={classes.nav_link}>
              <Link to={`/articles/${item.id}`}>{item.title}</Link>
              </li>))
          }
          <li className={classes.nav_link}><Link to="/counter">Counter</Link></li>
        </ul>
      </div>
    </div>
  );
};

export default HeaderBottom;
