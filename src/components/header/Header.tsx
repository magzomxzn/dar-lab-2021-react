import React, { useEffect, useState } from 'react';

import styles from './Header.module.scss';

import HeaderTop from './header-top/HeaderTop';
import HeaderBottom from './header-bottom/HeaderBottom';
import { useDispatch, useSelector } from 'react-redux';
import { selectCategories } from '../../shared/redux/categories/categories.selectors';
import { fetchCategories } from '../../shared/redux/categories/categories.actions';

const topHeaderItems = ["O нас", "Обучение", "Сообщество", "Медиа", "DAR Lab"];

type Props = {
 
}

const Header: React.FC<Props> = () => {
  
  const dispatch = useDispatch();
  const [topItems] = useState<string[]>(topHeaderItems);

  const categories = useSelector(selectCategories);
  console.log('CATEGORIES', categories)
  
  useEffect(() => {
    dispatch(fetchCategories());
  }, [])
  
  return (
    <header className={styles.layout_header}>
      <HeaderTop items={topItems} />
      <HeaderBottom items={categories} />
    </header>
  );
};

export default Header;
