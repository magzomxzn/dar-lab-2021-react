import React from 'react';
import styles from './HeaderTop.module.scss';
import Button from '../../button/Button';
import HeaderItems from './header-items/HeaderItems';
import Profile from '../../profile/Profile';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { selectProfile } from '../../../shared/redux/auth/auth.selectors';


type Props = {
  items: string[];
}

const HeaderTop: React.FC<Props> = ({items}) => {

  const profile = useSelector(selectProfile)

  const history = useHistory();

  const goToLogin = () => {
    history.push('/login');
  }

  return (
     <div className={styles.header_top}>
        <div className={styles.header_brand}>
          <div className={styles.header_logo} />
        </div>
        <div className={styles.header_top_menu}>
          <ul>
            <HeaderItems items={items} />
          </ul>
        </div>
        {
          profile ? <Profile username={profile.username} avatar={profile.avatar} />:
            <div className={styles.header_login}>
              <Button title={'Login'} outlined onClick={goToLogin} />
            </div>
        }
      </div>
  );
}

export default HeaderTop;
