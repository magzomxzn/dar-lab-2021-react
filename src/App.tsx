import React, { useEffect, useReducer } from 'react';
import { useDispatch } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import './App.scss';
import Counter from './components/counter/Counter';

import Header from './components/header/Header';
import ArticlePage from './pages/article/ArticlePage';
import ArticlesPage from './pages/articles/ArticlesPage';
import LoginPage from './pages/login/LoginPage';
import AppContext, { reducer } from './shared/app.context';
import { fetchProfile } from './shared/redux/auth/auth.actions';

const App: React.FC = () => {
  
  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem('authToken')
    if (token) {
      dispatch(fetchProfile());
    }
  }, [])

  return (
      <div className="App">
        <Header />
        <div className="container">
          <Switch>
            <Route path="/counter" render={(props) => <Counter initialCount={3} />} />
            <Route exact path="/articles" component={ArticlesPage} />
            <Route path="/articles/:categoryId" component={ArticlesPage} />
            <Route path="/article/:articleId" component={ArticlePage} />
            <Route path="/login" component={LoginPage} />
            <Route path="*">
              <Redirect to="/articles" />
            </Route>
          </Switch>
        </div>
      </div>
  );
}

export default App;
