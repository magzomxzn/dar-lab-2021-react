import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { ArticlesList } from '../../components/actual/Actual';
import { fetchArticles } from '../../shared/redux/articles/articles.actions';
import { selectArticles } from '../../shared/redux/articles/articles.selectors';

const ArticlesPage: React.FC = () => {

    const {categoryId} = useParams<{ categoryId: string }>();
  
    const dispatch = useDispatch();

    const articles = useSelector(selectArticles);

    useEffect(() => {
     dispatch(fetchArticles({categoryId}));
    }, [categoryId]);
    
    return ( 
        <div className="ArticlesPage">
            <ArticlesList articles={articles}/>
        </div>
    )
}

export default ArticlesPage;