FROM node:12-alpine as build-stage

WORKDIR /app
COPY . .
ARG ENV
RUN npm ci
RUN npm run build:${ENV}

FROM nginx:stable

COPY --from=build-stage /app/build /var/www
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]